package ru.tsc.panteleev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.data.DataXmlJaxbSaveRequest;
import ru.tsc.panteleev.tm.event.ConsoleEvent;

@Component
public class DataXmlJaxbSaveListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Save data in xml file";

    @NotNull
    private static final String NAME = "data-save-xml-jaxb";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlJaxbSaveListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        showDescription();
        getDomainEndpoint().saveDataXmlJaxb(new DataXmlJaxbSaveRequest(getToken()));
    }

}
