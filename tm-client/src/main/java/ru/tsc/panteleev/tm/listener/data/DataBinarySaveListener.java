package ru.tsc.panteleev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.data.DataBinarySaveRequest;
import ru.tsc.panteleev.tm.event.ConsoleEvent;

@Component
public class DataBinarySaveListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Save data in binary file";

    @NotNull
    private static final String NAME = "data-save-bin";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBinarySaveListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        showDescription();
        getDomainEndpoint().saveDataBinary(new DataBinarySaveRequest(getToken()));
    }

}
