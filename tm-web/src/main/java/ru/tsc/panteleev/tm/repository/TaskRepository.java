package ru.tsc.panteleev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

@Repository
public class TaskRepository {

    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        for (int i = 1; i < 11; i++) {
            add(new Task("Task " + i, UUID.randomUUID().toString()));
        }
    }

    public void create() {
        add(new Task(("Task " + System.currentTimeMillis() / 1000), UUID.randomUUID().toString()));
    }

    public void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(@NotNull final Task task) {
        tasks.replace(task.getId(), task);
    }


    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

}
