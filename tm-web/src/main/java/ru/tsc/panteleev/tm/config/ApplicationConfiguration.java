package ru.tsc.panteleev.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.panteleev.tm")
public class ApplicationConfiguration {

}
