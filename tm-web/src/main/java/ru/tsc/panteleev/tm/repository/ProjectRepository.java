package ru.tsc.panteleev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

@Repository
public class ProjectRepository {

    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        for (int i = 1; i < 11; i++) {
            add(new Project("Project " + i, UUID.randomUUID().toString()));
        }
    }

    public void create() {
        add(new Project(("Project " + System.currentTimeMillis() / 1000), UUID.randomUUID().toString()));
    }

    public void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void save(@NotNull final Project project) {
        projects.replace(project.getId(), project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

}
