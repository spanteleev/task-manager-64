package ru.tsc.panteleev.tm.dto.response.user;


import lombok.NoArgsConstructor;
import ru.tsc.panteleev.tm.dto.model.UserDto;

@NoArgsConstructor
public class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse(UserDto user) {
        super(user);
    }

}
