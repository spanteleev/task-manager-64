package ru.tsc.panteleev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.api.service.*;
import ru.tsc.panteleev.tm.endpoint.*;
import ru.tsc.panteleev.tm.listener.EntityListener;
import ru.tsc.panteleev.tm.listener.OperationEventListener;
import ru.tsc.panteleev.tm.util.SystemUtil;
import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public final class Bootstrap {

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private Backup backup;

    @Autowired
    private AbstractEndpoint[] endpoints;

    public void registry(Object endpoint) {
        final String host = getPropertyService().getServerHost();
        final String port = getPropertyService().getServerPort().toString();
        final String name = endpoint.getClass().getSimpleName();
        final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
    }

    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    public void initJMS() {
        BasicConfigurator.configure();
        EntityListener.setConsumer(new OperationEventListener());
    }

    private void runStartupOperation() {
        initPID();
        initJMS();
        initEndpoints();
        backup.start();
        registryShutdownHookOperation();
        loggerService.info("** TASK-MANAGER SERVER IS STARTING **");
    }

    private void registryShutdownHookOperation() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER SERVER IS SHUTTING DOWN **");
                backup.stop();
            }
        });
    }

    public void run() {
        runStartupOperation();
    }

}
